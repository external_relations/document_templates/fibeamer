#!/bin/bash
[[ ! -e VERSION.tex ]] && exit 1
VERSION="$(<VERSION.tex \
  sed -rn 's#(....)/(..)/(..) v([^ ]*) fibeamer MU beamer theme#v\4#p')"
[[ -z "$VERSION" ]] && exit 2

git tag "$VERSION"
git push all master --tags --force
