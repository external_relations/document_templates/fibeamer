#!/bin/bash
# Prepares the Overleaf archives for every faculty

# Clean up
rm -rf overleaf
rm -rf /tmp/{fibeamer,overleaf} ./overleaf &&
# Prepare the files
mkdir /tmp/{fibeamer,overleaf} &&
unzip -d /tmp/fibeamer fibeamer.tds.zip &&
TDS=/tmp/fibeamer/tex/latex/fibeamer
for fac in mu-econ mu-fi mu-fsps mu-fss mu-law mu-med mu-ped mu-phil mu-sci bs-standard; do
  OVERLEAF=/tmp/overleaf/$fac
  uni=${fac%%-*}
  fac=${fac#*-}
  mkdir -p "$OVERLEAF"/fibeamer &&
  cp -L -v -r example/{$uni/$fac-*.tex,$uni/resources} "$OVERLEAF" &&
  rm "$OVERLEAF"/resources/DESCRIPTION &&
  tar c "$TDS"/beamerthemefibeamer.sty \
        "$TDS"/theme/$uni/beamer{font,inner,outer,color}themefibeamer-$uni.sty \
        "$TDS"/theme/$uni/beamercolorthemefibeamer-$uni-$fac.sty \
        "$TDS"/logo/$uni/fibeamer-$uni-$fac-*.pdf | tar xvC "$OVERLEAF" &&
  mv "$OVERLEAF$TDS"/* "$OVERLEAF"/fibeamer &&
  mv "$OVERLEAF"/fibeamer/beamerthemefibeamer.sty "$OVERLEAF"/beamerthemefibeamer.sty &&
  rm -rf "$OVERLEAF"/tmp &&
  printf "\$ENV{'TZ'}='Europe/Prague';\n" > "$OVERLEAF"/latexmkrc &&
  (cd "$OVERLEAF" && zip -r ../$uni-$fac.zip .)
done &&
rm -rf /tmp/fibeamer &&
mv /tmp/overleaf .
